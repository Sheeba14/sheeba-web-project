package com.training.web.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.training.web.model.User;

public class UserDao {
	
	public List<User> getUserList(){
		
		List <User> userList =new ArrayList<>();
		
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			
            String url = "jdbc:sqlserver://localhost:1433;"+
                    "databaseName=db01";
            
            String user = "sa";
            
            String pwd = "admin";
            
            Connection con = DriverManager.getConnection(url, user, pwd);
            
            Statement stmt = con.createStatement();            
			
            ResultSet rs = stmt.executeQuery("select * from user1");
            
            
            
            while (rs.next()) {
            	User usr = new User();
            	usr.setUserID(rs.getString(1));
            	usr.setUserName(rs.getString(2));
            	usr.setPassword(rs.getString(3));
            	userList.add(usr);
            }
            
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return userList;
	}

}
